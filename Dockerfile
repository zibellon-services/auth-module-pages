FROM node:14.18.2-alpine

WORKDIR /app

#Подготовка к установке зависимостей 
COPY package.json yarn.lock ./
# Установка зависимостей
RUN yarn install --frozen-lockfile

# Копируем все файлы
COPY .editorconfig ./
COPY jsconfig.json ./
COPY nuxt.config.js ./
COPY assets ./assets/
COPY components ./components/
COPY pages ./pages/
COPY static ./static/
COPY utils ./utils/

# Сборка проекта
RUN yarn build

#Команда для запуска сервера внутри контейнера
CMD ["yarn", "start"]
