const CONSTANTS = {
  AUTH_PASSWORD_RESET_LINK_SEND: (SERVER_URL) => `${SERVER_URL}/api/v1/auth/password/reset/link/send`,
  AUTH_PASSWORD_RESET_CONFIRM: (SERVER_URL) => `${SERVER_URL}/api/v1/auth/password/reset/confirm`,
};

export { CONSTANTS };
